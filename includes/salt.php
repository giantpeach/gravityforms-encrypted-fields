<?php
// This unique salt is generated from your wordpress security keys.
// Prevent direct access to this file.
defined( "ABSPATH" ) or die();

function gfef_get_salt() {
    $salt = "[%%SALT%%]";
    if (strpos($salt, "%%SALT%%")) {
        $salt = false;
    } 
    return $salt;
}